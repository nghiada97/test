package Base;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;

public class ExcelUtil {

    public static XSSFWorkbook workbook = null;
    public static XSSFSheet sheet1;

    public static Object[][] ExcelData(String url, int sheet){

        File file = new File(url);
        try {
             workbook = new XSSFWorkbook(file);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

        sheet1 = workbook.getSheetAt(sheet);

        Object[][] data = new Object[sheet1.getLastRowNum()][sheet1.getRow(0).getLastCellNum()];

        for(int i=0; i < sheet1.getLastRowNum(); i++){
            for (int j=0; j< sheet1.getRow(0).getLastCellNum(); j++){
                if (sheet1.getRow(i+1).getCell(j).getCellTypeEnum() == CellType.STRING){
                data[i][j] = sheet1.getRow(i+1).getCell(j).getStringCellValue();
            } else if(sheet1.getRow(i+1).getCell(j).getCellTypeEnum() == CellType.NUMERIC){
                    data[i][j] = sheet1.getRow(i+1).getCell(j).getNumericCellValue();
                }
            }
        }
        return data;
    }

}
