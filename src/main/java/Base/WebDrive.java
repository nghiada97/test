package Base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.apache.hc.core5.util.Asserts;
import org.openqa.selenium.*;
import org.openqa.selenium.Point;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class WebDrive {

    WebDriver driver;

    public WebDrive(){

    }

    public WebDriver getDriverChrome(){

        if(driver == null) {
            WebDriverManager.chromedriver().clearDriverCache();
            WebDriverManager.chromedriver().setup();
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--allow-running-insecure-content");
            chromeOptions.addArguments("--ignore-certificate-errors");
            chromeOptions.addArguments("--disable-web-security");
            chromeOptions.addArguments("disable-infobars");
            chromeOptions.addArguments("window-size=800,600");
            driver = new ChromeDriver(chromeOptions);
//            driver.manage().window().maximize();
        }
        return driver;
    }

    public WebDriver getDriverFireFox(){
        if(driver == null){
            WebDriverManager.firefoxdriver().clearDriverCache();
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.addArguments("--allow-running-insecure-content");
            firefoxOptions.addArguments("--ignore-certificate-errors");
            firefoxOptions.addArguments("--disable-web-security");
            firefoxOptions.addArguments("disable-infobars");
//            firefoxOptions.addArguments("window-size=800,600");
            driver = new FirefoxDriver(firefoxOptions);
        }
        return driver;
    }

    public void quit() {
        try {
//            driver.close();
            driver.quit();
        } catch (Exception e) {
            throw e;
        }
    }
    public void sendkey(By locator, String sendkey) {
        try {
            fluentWaitForElement(locator).sendKeys(sendkey);
        } catch (Exception exception) {
            throw exception;
        }
    }

    public void ClearText(By by){
        fluentWaitForElement(by).clear();
    }

    public void Upload(By by, String sendkey){
        driver.findElement(by).sendKeys(sendkey);
    }

    public void OpennewTab(){
        ((JavascriptExecutor)driver).executeScript("window.open()");
    }

    public void SwitchTab(int i){
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(i));
    }

    public WebElement fluentWaitForElement(By by) {
        try {
            Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(15))
                    .pollingEvery(Duration.ofMillis(500))
                    .ignoring(NoSuchElementException.class);
            WebElement element = wait.until(new Function<WebDriver, WebElement>() {
                public WebElement apply(WebDriver driver) {
                    WebElement e = driver.findElement(by);
                    if (!e.isDisplayed())
                        throw new NoSuchElementException("");
                    return e;
                }
            });
            return element;
        } catch (Exception exception) {
            throw exception;
        }
    }

    public void click(By locator) {
        try {
            fluentWaitForElement(locator).click();
        } catch (Exception exception) {
            throw exception;
        }
    }
    public String getText(By locator){
        try {
            fluentWaitForElement(locator).getText();
        } catch (Exception e){
            throw e;
        }
        return fluentWaitForElement(locator).getText();
    }

//    public String RemoveChar(String str , int n, int m){
//        String front = str.substring(n,m);
//        return front;
//    }

    public void DragAndDrop(By a, By b) {
        try {
            Actions act = new Actions(driver);
            WebElement elementa = driver.findElement(a);
            WebElement elementb = driver.findElement(b);
            act.dragAndDrop(elementa, elementb).build().perform();
        } catch (Exception e){
            throw e;
        }
    }
    public boolean displayed(By by){
        try {
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            WebElement element = driver.findElement(by);
            return element.isDisplayed();
        } catch (Exception e){
            throw e;
        }
        finally {
            driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        }
    }



    public void DropDown(By by, int i){
        Select select = new Select(driver.findElement(by));
        select.selectByIndex(i);

    }
    public int ElementSize(By by){
        return driver.findElements(by).size();
    }


    public void Enter(By by){
        driver.findElement(by).sendKeys(Keys.ENTER);
    }


    // Screenshot with webDrive
   public File Screenshot(By by, String a) {

        WebElement element = driver.findElement(by);
       File imageLocation = new File(a);

        File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
       BufferedImage fullimage = null;
       try {
           fullimage = ImageIO.read(file);
           Point point = element.getLocation();

           int eleWight = element.getSize().width;
           int eleHeight = element.getSize().height;
           System.out.println(eleHeight);
           System.out.println(eleWight);
           System.out.println(point.getX());
           System.out.println(point.getY());

           BufferedImage eleSreenshot = fullimage.getSubimage(point.getX(),point.getY(),eleWight,eleHeight);
           ImageIO.write(eleSreenshot,"png",file);

           FileUtils.copyFile(file,imageLocation);
       } catch (IOException e) {
           e.printStackTrace();
       }

       return imageLocation;
   }
   
    // Compare 2 similar Image
   public void CompareImage(String url1, String Url2) throws IOException {

           BufferedImage BeforeImage = ImageIO.read(new File(url1));
           BufferedImage AfterImage = ImageIO.read(new File(Url2));

            ImageDiffer imageDiffer = new ImageDiffer();
            ImageDiff diff = imageDiffer.makeDiff(BeforeImage,AfterImage);

            boolean ImageDiffirent = diff.hasDiff();
            if(ImageDiffirent == true){
                Asserts.check(true,"True");
            } else {
                Asserts.check(false,"False");
            }
   }

   // Compare size image
    public boolean CompareSize(String imageSmall1, String imageLarge1){

        try {
            BufferedImage imageSmall = null;
            BufferedImage imageLarge = null;
            File fileA = new File(imageSmall1);
            File fileB = new File(imageLarge1);

            imageSmall = ImageIO.read(fileA);
            imageLarge = ImageIO.read(fileB);

                int Width1 = imageSmall.getWidth();
                int Width2 = imageLarge.getWidth();
                int height1 = imageSmall.getHeight();
                int height2 = imageLarge.getHeight();

                if (Width2 > Width1 && height2 > height1){
                    return true;
        } else {
                    return false;
                }

         } catch (IOException e){
            return false;
        }

    }

    // Parse String to Double
   public double parsdouble(String str){
        double a = 0;
        try {
            String b = str.substring(1,str.length());
            a = Double.parseDouble(b);
        } catch (Exception e){
            throw e;
        }
        return a;
   }
    // Get text with Attribute
   public int getAttribute (By by ) {
        int i = Integer.parseInt(driver.findElement(by).getAttribute("value"));
        return  i;
   }

   public int GetHeight(By by){
       return driver.findElement(by).getSize().height;
   }
    public int GetWidth(By by){
        return driver.findElement(by).getSize().width;
    }

    public void SwitchWindown(){
        String mainWindown = driver.getWindowHandle();
        Set<String> set = driver.getWindowHandles();
        Iterator<String> itr = set.iterator();
        while (itr.hasNext()){
            String childWindown = itr.next();
            if (!mainWindown.equals(childWindown)){
                driver.switchTo().window(childWindown);
            }
        }

    }

    public void MoveMouse(By by){
        Actions actions = new Actions(driver);
        actions.moveToElement(fluentWaitForElement(by)).perform();
    }

    public void ScrollUpAndDown(int form, int to){
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy("+form+","+to+")");
    }
}

