package models;

import java.util.ArrayList;
import java.util.List;

public class Order {

    List<CartIteam> products ;

    double SubtotalPrice;

    double CurrentPrice;
    int ShippingPrice;

    double totalPrice;

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getSubtotalPrice() {
        return SubtotalPrice;
    }

    public void setSubtotalPrice(double subtotalPrice) {
        SubtotalPrice = subtotalPrice;
    }



    public Order (){
        products = new ArrayList<>();
        ShippingPrice = 2;
    }

    public double CalPrice(){
        double result = 0;
        for (CartIteam cartIteam: products){
            result += cartIteam.getQty() * cartIteam.getPrice();
            System.out.println(result);
        }
        return result;
    }

    public void addProduct(CartIteam cartIteam){
        this.products.add(cartIteam);
        this.SubtotalPrice = CalPrice();
        this.CurrentPrice = SubtotalPrice + ShippingPrice;
    }


    public List<CartIteam> getProducts() {
        return products;
    }

    public void setProducts(List<CartIteam> products) {
        this.products = products;
    }

    public double getCurrentPrice() {
        return CurrentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        CurrentPrice = currentPrice;
    }
}
