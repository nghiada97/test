package TestCase1;

import Base.WebDrive;
import org.openqa.selenium.By;
import org.testng.Assert;

public class ContactUsPage {

    WebDrive drive;

    private static final By SubjectHeading = By.xpath("//select[@name=\"id_contact\"]");
    private static final By EmailAddress = By.xpath("//input[@id=\"email\"]");
    private static final By attachFile = By.xpath("//*[@id=\"fileUpload\"]");
    private static final By sendButton = By.xpath("//button[@name=\"submitMessage\"]");
    private static final By MesageFiels = By.xpath("//textarea[@name=\"message\"]");
    private static final By Mess = By.xpath("//p[@class=\"alert alert-success\"]");


    public ContactUsPage(WebDrive drive){
        this.drive = drive;
    }

    public void SendMessage(){
        drive.DropDown(SubjectHeading,1);
        drive.sendkey(EmailAddress,"moneylove9971@gmail.com");

        drive.sendkey(MesageFiels, "Mai Mai");
        drive.Upload(attachFile,"C:\\Users\\LQA.CA0619GT39\\Downloads\\145073884_2846244982300797_2228101626056035905_n (1).jpg");
        drive.click(sendButton);
    }

    public void SuccessfullyMess(){
        Assert.assertEquals(drive.getText(Mess),"Newsletter : You have successfully subscribed to this newsletter.");
    }


}
