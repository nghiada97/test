package TestCase1;

import Base.WebDrive;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class HomePage {

    WebDrive drive;


    private static final By SignInButton = By.xpath("//a[@class=\"login\"]");
    private static final By NewsletterFiels = By.xpath("//input[@id=\"newsletter-input\"]");
    private static final By submitButton = By.xpath("//button[@name=\"submitNewsletter\"]");
    private static final By NewsletterMess = By.xpath("//p[@class=\"alert alert-success\"]");
    private static final By ContactUs = By.xpath("//div[@id=\"contact-link\"]/a");
    private static final By SearchText = By.xpath("//input[@value=\"search\"]");
    private static final By SearchBar = By.xpath("//input[@name=\"search_query\"]");
    private static final By ac_resFiel = By.xpath("//div[@class=\"ac_results\"]//li[1]");
    private static final By SearchButton = By.xpath("//button[@name=\"submit_search\"]");
    private static final By continueShoppingButton = By.xpath("//span[@title=\"Continue shopping\"]/span");
    private static final By ProceedButton = By.xpath("//a[@title=\"Proceed to checkout\"]");
    private static final By ShoppingCartButton = By.xpath("//div[@class=\"shopping_cart\"]/a");
    private static final By image1 = By.xpath("//ul[@id=\"homefeatured\"]//li[1]//img");
    private static final By LogoHome = By.xpath("//img[@class=\"logo img-responsive\"]");


    private  By ac_Result (int i) {
       return By.xpath("//div[@class=\"ac_results\"]//li["+i+"]/strong"); }

    private  By ProductAddtoCart (int i){
        return By.xpath("//ul[@id=\"homefeatured\"]//li["+i+"]//div[@class=\"product-container\"]//div[@class=\"button-container\"]/a[@title=\"Add to cart\"]");
    }

    private  By Price (int i) {
        return By.xpath("//ul[@id=\"homefeatured\"]//li["+i+"]//div[@class=\"right-block\"]//span[@class=\"price product-price\"]");
    }

    private  By CurrentPrice (int i) {
        return By.xpath("//ul[@id=\"homefeatured\"]//li["+i+"]//div[@class=\"right-block\"]//span[@class=\"old-price product-price\"]");
    }

    private By image (int i){
        return By.xpath("//ul[@id=\"homefeatured\"]//li["+i+"]//img");
    }


    //button[@name="submit_search"]
    //input[@name="search_query"]
    //input[@value="search"]

    public HomePage(WebDrive drive){
        this.drive = drive;
    }

    public void ClickLogo(){
        drive.click(LogoHome);
    }


    public void ClickSignIn(){
        drive.click(SignInButton);
    }

    public void SubmitNewsletter(){

        drive.sendkey(NewsletterFiels,"moneylove9971@gmail.com");
        drive.click(submitButton);
    }

    public void SuccessfullyMess(){
        Assert.assertEquals(drive.getText(NewsletterMess)," Newsletter : You have successfully subscribed to this newsletter.");
    }

    public void clickContactUs(){
        drive.click(ContactUs);
    }

//    public void searchBarCheckText(){
//        drive.displayed(SearchText);
//        drive.disEnabled(SearchText);
//    }

    public void EnterSearchBar(String text){
        drive.sendkey(SearchBar,text);
    }
    public void ClearText(){
        drive.ClearText(SearchBar);
    }

    public void Sreenshot(String ScreenName){
        drive.Screenshot(SearchBar, ScreenName);
    }



    public void CheckSuggestionsResult(String text){
        for(int i=1; i <= 5 ;i++){
            System.out.println(drive.getText(ac_Result(i)));
            Assert.assertEquals(drive.getText(ac_Result(i)).toLowerCase(Locale.ROOT),text);
        }
    }

    public void ClickSuggestionResult(){

        drive.click(SearchButton);

    }

    public void AddtocartAndContinue (int productNumber){
        drive.ScrollUpAndDown(0,750);
        for(int i=1; i <= productNumber; i++ ){
            drive.MoveMouse(image(i));
            drive.click(ProductAddtoCart(i));
            drive.click(continueShoppingButton);
        }
    }

    public void AddtocartAndProceed (){
        drive.MoveMouse(image(7));
        drive.click(ProductAddtoCart(7));
        drive.getDriverChrome().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        drive.click(ProceedButton);
    }

    public void ClickShoppingCart (){
        drive.click(ShoppingCartButton);
    }

    public void ClickImage(){
        drive.click(image1);
    }


    public void ClickDiscountProduct(){

        drive.ScrollUpAndDown(0,750);
        drive.MoveMouse(image(7));
//        drive.getDriverFireFox().manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
        drive.click(ProductAddtoCart(7));
        drive.click(ProceedButton);

    }

}
