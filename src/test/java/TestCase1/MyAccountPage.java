package TestCase1;

import Base.WebDrive;
import org.openqa.selenium.By;
import org.testng.Assert;

public class MyAccountPage {

    WebDrive drive;

    private static final By MyaccountText = By.xpath("//p[@class=\"info-account\"]");

    public MyAccountPage(WebDrive drive){
        this.drive = drive;
    }

    public void ShowMyaccount(){
        drive.displayed(MyaccountText);
    }
}
