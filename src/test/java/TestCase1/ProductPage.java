package TestCase1;

import Base.WebDrive;
import models.CartIteam;
import org.openqa.selenium.By;
import org.testng.Assert;

public class ProductPage {

    WebDrive drive;

    private static final By image = By.xpath("//span/img[@itemprop=\"image\"]");
    //span[@id="view_full_size"]
    //span/img[@itemprop="image"]
    private static final By imageLarge = By.xpath("//div[@class=\"fancybox-inner\"]");
    private static final By imageLargeName = By.xpath("//span[@class=\"child\"]");
    private static final By ProductName = By.xpath("//h1[@itemprop=\"name\"]");
    private static final By QtyField = By.xpath(" //input[@name=\"qty\"]");
    private static final By SubmitButton = By.xpath("//button[@name=\"Submit\"]");
    private static final By NullMessText = By.xpath("//p[@class=\"fancybox-error\"]");
    private static final By MessClose = By.xpath("//a[@title=\"Close\"]");
    private static final By SuccessMess = By.xpath("//div[@class=\"layer_cart_product col-xs-12 col-md-6\"]/h2");
    private static final By CloseWindow = By.xpath("//span[@title=\"Close window\"]");
    private static final By ShoppingCartButton = By.xpath("//div[@class=\"shopping_cart\"]/a");
    private static final By TwitterButton = By.xpath("//button[contains(@class ,' btn-twitter')]");
    private static final By WriteCommentButton = By.xpath("//a[@id=\"new_comment_tab_btn\"]");
    private static final By TitleComment = By.xpath("//input[@id=\"comment_title\"]");
    private static final By Content = By.xpath("//textarea[@name=\"content\"]");
    private static final By SendButton = By.xpath("//button[@name=\"submitMessage\"]");
    private static final By MessNewComment = By.xpath("//div[@class=\"fancybox-inner\"]");
    private static final By SendToAFriend = By.xpath("//a[@id=\"send_friend_button\"]");
    private static final By NameofFriend = By.xpath("//input[@id=\"friend_name\"]");
    private static final By EmailofFriend = By.xpath("//input[@id=\"friend_email\"]");
    private static final By SendEmailButton = By.xpath("//button[@id=\"sendEmail\"]");
    private static final By MessSentEmailSuccess = By.xpath("//div[@class=\"fancybox-inner\"]/p[1]");
    private static final By imageClose = By.xpath("//a[@title=\"Close\"]");
    private static final By ViewLargeButton = By.xpath("//span[@class=\"span_link no-print\"]");


    //span[@id="view_full_size"]/img

    public ProductPage(WebDrive drive){
        this.drive = drive;
    }

    public void ClickImage(){
        drive.click(image);
    }

    public void ClickViewLarge(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        drive.click(ViewLargeButton);
    }

    public void ImageS(String a){
        drive.Screenshot(image,a);
    }
    public void ImageL(String a){
        drive.fluentWaitForElement(imageLarge);
        drive.Screenshot(imageLarge,a);
    }
    public void ImageL1 (String a){
        drive.Screenshot(imageLarge,a);
    }

    public void CompareImageSize(){
        System.out.println(drive.GetWidth(image));
        System.out.println(drive.GetHeight(image));

        drive.click(image);
        System.out.println(drive.GetWidth(imageLarge));

        System.out.println(drive.GetHeight(imageLarge));

    }

    public void CompareName(){
        String productName = drive.getText(ProductName);
        String ImageName = drive.getText(imageLargeName);
        Assert.assertEquals(productName,ImageName);
        drive.click(imageClose);
    }

    public void ChangeQtyAndAdd(String qty){
        drive.ClearText(QtyField);
        drive.sendkey(QtyField,qty);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        drive.click(SubmitButton);
    }

    public void NullQtyMess(){
        Assert.assertEquals(drive.getText(NullMessText),"Null quantity.");
        drive.click(MessClose);
    }

    public void SuccessAdd(){
        Assert.assertEquals(drive.getText(SuccessMess),"Product successfully added to your shopping cart");
        drive.click(CloseWindow);
        drive.click(ShoppingCartButton);
    }

    public CartIteam Iteam (){
        CartIteam cartIteam = new CartIteam();
        cartIteam.setName(drive.getText(ProductName));
        return cartIteam;
    }

    public void ClickTwitter(){
        drive.click(TwitterButton);
    }

    public void WriteAComment(){
        drive.click(WriteCommentButton);
        drive.sendkey(TitleComment,"MaiMai");
        drive.sendkey(Content,"MaiMai");
        drive.click(SendButton);
        drive.displayed(MessNewComment);
    }

    public void SendAFriend(){
        drive.click(SendToAFriend);
        drive.sendkey(NameofFriend,"Mai");
        drive.sendkey(EmailofFriend,"nghiada97@gmail.com");
        drive.click(SendEmailButton);
        Assert.assertEquals(drive.getText(MessSentEmailSuccess),"Your e-mail has been sent successfully");
    }
}
