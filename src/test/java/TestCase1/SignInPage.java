package TestCase1;

import Base.WebDrive;
import org.openqa.selenium.By;
import org.testng.Assert;

public class SignInPage {

    WebDrive drive;

    private static final By CreateEmailFiel = By.xpath("//input[@name=\"email_create\"]");
    private static final By CreateAccountButton = By.xpath("//button[@name=\"SubmitCreate\"]");
    private static final By ErrorMess = By.xpath("//div[@id=\"create_account_error\"]//li");
    private static final By SigninEmailField = By.xpath("//div[@class=\"form_content clearfix\"]//input[@name=\"email\"]");
    private static final By PasswordField = By.xpath("//input[@id=\"passwd\"]");
    private static final By SigninButton = By.xpath("//button[@id=\"SubmitLogin\"]");


    public SignInPage(WebDrive drive){
        this.drive = drive;
    }

    public void CreateAccount(String Email){
        drive.sendkey(CreateEmailFiel, Email);
        drive.click(CreateAccountButton);
    }

    public void ShowErrorMess(){

        Assert.assertEquals(drive.getText(ErrorMess),"Invalid email address.");
    }

    public void Signin(){
        drive.sendkey(SigninEmailField,"moneylove9971@gmail.com");
        drive.sendkey(PasswordField, "12345");
        drive.click(SigninButton);
    }
}
